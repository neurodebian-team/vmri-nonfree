#!/bin/sh

set -u
set -e

zipsrc=$1

curdir=$(pwd)
wdir=$(mktemp -d)
echo $wdir

unzip -qq -d $wdir $zipsrc

cd ${wdir}/*

#clean
rm -f *.bat *.html *.mf *.mf *.xml *.txt
rm -rf *.app

# determine version
version="$(grep 'Version ' _Read_Me.htm |head -n1 | sed -e 's/.*Version //g' -e 's,</B>.*,,g')"

cd ..
mv * vmri-nonfree-${version}
tar -czf ${curdir}/vmri-nonfree_${version}.tar.gz *

cd ${curdir}
rm -r ${wdir}
